# Extreme Orders app

## Hosted [here](https://extreme.tvangelov.com/)

## Project description

The purpose of this app is to make the process of purchasing products for the existing separate departments like Kitchen, Service, Bar, Fruits inside a club bar. Each department products purchase is handled by a different provider. There is a main menu to select a specific department which leads to a text area form for a specific date which can be selected from the calendar above the textarea. The textarea is empty by default but if there is an existing products purchase for this date the current products must be also  displayed. Each new input must update the whole list keeping the new and the old data. Frontend is developed by Vue.js the backend is implemented as an API made on Laravel.
