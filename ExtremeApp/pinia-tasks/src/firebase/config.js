import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyB8bEDpMl48zBqWMKX7jVwaCMOFwn2XEqc',
  authDomain: 'extreme-orders.firebaseapp.com',
  projectId: 'extreme-orders',
  storageBucket: 'extreme-orders.appspot.com',
  messagingSenderId: '352385863559',
  appId: '1:352385863559:web:1fc7d25b2c074945ce8d22',
  databaseURL:
    'https://extreme-orders-default-rtdb.europe-west1.firebasedatabase.app/',
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
