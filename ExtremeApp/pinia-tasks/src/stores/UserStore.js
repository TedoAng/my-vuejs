import { defineStore } from 'pinia';

export const useUserStore = defineStore('userStore', {
  state: () => ({
    id:null, 
    displayName:null
  }),
  getters: {
    userId() {
      return this.id;
    },
    userName() {
      return this.displayName
    }
  },
  actions: {
    setId(value) {
      this.id = value ;
    },
    setDisplayName(value){
      this.displayName = value;
    }
  },
});
