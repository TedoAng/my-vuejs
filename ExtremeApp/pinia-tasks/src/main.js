import { createPinia } from 'pinia';
import { createApp } from 'vue';
import router from './router';
import App from './App.vue';
import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'
import './assets/main.css';
// Import our custom CSS
import './scss/styles.scss'
// Import all of Bootstrap's JS
// import * as bootstrap from 'bootstrap'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserSecret, faLemon, faPrint, faPenToSquare } from '@fortawesome/free-solid-svg-icons'


library.add(faUserSecret)
library.add(faLemon)
library.add(faPrint)
library.add(faPenToSquare)

const app = createApp(App);


app.use(router);
app.use(createPinia());
app.component('Datepicker', Datepicker);
app.component('font-awesome-icon', FontAwesomeIcon)

app.mount('#app');
