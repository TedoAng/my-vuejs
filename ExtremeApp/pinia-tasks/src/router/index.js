import { createRouter, createWebHistory } from 'vue-router';
import Admin from '../pages/Admin.vue'
import NotFound from '../pages/NotFound.vue'
import SelectionMenu from '../components/SelectionMenu.vue'
import ViewPrint from '../components/ViewPrint.vue'
import Login from '../components/Login.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'Select',
            component: SelectionMenu
        },
        {
            path: '/admin',
            name: 'Admin',
            component: Admin
        },
        {
            path: '/print/:id',
            name: 'Print',
            component: ViewPrint
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        { path: '/*', component: NotFound }
    ]
});

export default router;
