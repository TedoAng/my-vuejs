import {
  ref,
  push,
  get,
  set,
  update,
  child,
  query,
  onValue,
  limitToLast,
  orderByValue,
  orderByKey,
  orderByChild,
  equalTo
} from 'firebase/database';
import { db } from '../firebase/config';

export const addList = async (orderFor, content, date) => {
    if (!content){
        throw new Error('Can not add nothing!');
    }
    const dashDate = date.split('/').join('-');
    const subjectUnit = await get(query(ref(db, `lists/${dashDate}/${orderFor}`)));
    const objectUnit = await get(query(ref(db, `lists/${dashDate}`)));
    const options = {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };

    let itemPack = {};
    const updates = {};

    if (objectUnit.val()) {
        console.log('has');
        
        updates[`lists/${dashDate}/${orderFor}`] = content;
    } else {
        console.log('has NOT');

        itemPack = {
            [orderFor]: content,
            created: new Intl.DateTimeFormat('en-GB').format(Date.now()),
            orderForDate: date,
            time: new Intl.DateTimeFormat('en-GB', options).format(new Date())
        };

        updates[`lists/${dashDate}`] = itemPack;
    }

    update(ref(db), updates);
};

export const getOrderByDate = async (date) => {
    console.log(date);
    const dashDate = date.split('/').join('-');
    const snapshot = await get(query(ref(db, `lists/${dashDate}`)));
    if(snapshot.val()) {
        return snapshot.val();
    }else {
        throw new Error('no result')
    }
    
};

export const getAllOrders = async () => {
    const snapshot = await get(query(ref(db, 'lists')));
    return Object.values(snapshot.val());
};

export const delDeleteList = async (id) => {
    if (id) {
        const updates = {};
        updates[`lists/${id}/`] = null;

        update(ref(db), updates);

        return 'List is Deleted!';
    } else {
        return 'Nothing to delete!';
    }
};

export const updateDepartment = async(content, department, date) => {
    const dashDate = date.split('/').join('-');
    const updates = {};
    updates[`lists/${dashDate}/${department}`] = content;

    update(ref(db), updates);
}

export const getDepartmentByDate = async(department, date) => {
    const dashDate = date.split('/').join('-');
    const snapshot = await get(query(ref(db, `lists/${dashDate}/${department}`)));

    return snapshot.val();
}

export const setPhoneNumber = async (department, phone) => {
    const updates = {};
    updates[`phones/${department}`] = phone;

    update(ref(db), updates);
}

export const getPhoneNumber = async () => {
    const snapshot = await get(query(ref(db, `phones`)));

    return snapshot.val();
}
