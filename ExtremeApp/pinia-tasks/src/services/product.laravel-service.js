import axios from "axios";

const timeFromMSQLtoApp = (value) => {
    return value.split('-').reverse().join('/');
}

export const getAllOrders = async () => {
    const resp = await axios.get('https://listing.tvangelov.com/api/show');
    //Reformat the order_for_date prop
    const respRework = resp.data.map(order =>({...order, order_for_date : timeFromMSQLtoApp(order.order_for_date)}));

    if (respRework.length) {
        return respRework;
    } else {
        throw new Error('No result')
    }
};

export const getOrderByDate = async (value) => {
    const splitDate = value.split('/');
    const newDate = `${splitDate[2]}-${splitDate[1]}-${splitDate[0]}`
    const resp = await axios.get(`https://listing.tvangelov.com/api/show/${newDate}`);
    //Reformat the order_for_date prop
    const respRework = resp.data.map(order =>({...order, order_for_date : timeFromMSQLtoApp(order.order_for_date)}));
    return respRework;
};

export const addList = async (orderFor, content, date) => {

    const url = 'https://listing.tvangelov.com/api/create';
    const body = {
        [orderFor]: content,
        order_for_date: date.split('/').reverse().join('-')
    };
    const headers = {
        headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    try {
        const { data } = await axios.post(url, body, headers);
        return (200);
    } catch (error) {
        throw new Error(`${error.response.data}`);
    }
};

export const getDepartmentByDate = async (orderFor, date) => {
    const url = `https://listing.tvangelov.com/api/show/${orderFor}/${date.split('/').reverse().join('-')}`;

    try {
        const { data } = await axios.get(url);
        return data[0] ? data[0] : '' ;
    } catch (error) {
        console.log('Error: ',error.response.data);
    }
 }


